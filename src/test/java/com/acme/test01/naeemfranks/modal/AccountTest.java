package com.acme.test01.naeemfranks.modal;

import com.acme.test01.naeemfranks.config.SystemDB;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.UUID;

class AccountTest {
    private final long testAcctId = 1L;
    private Account account;

    @BeforeEach
    void setUp() {
        account = SystemDB.getAccount(testAcctId);
    }

    @Test
    void getAccountId() {
        Assert.isTrue(account.getAccountId() == testAcctId, "Account ID does not match");
    }

    @Test
    void getCustomerNumber() {
        Assert.hasText(account.getCustomerNumber(), "Customer Number empty!");
    }

    @Test
    void getBalance() {
        Assert.isTrue(account.getBalance() == 2000, "Account balance does not match");
    }

    @Test
    void initializeSavingsAccount() {
        SavingsAccount acct = new SavingsAccount(10L, UUID.randomUUID().toString(), 2000);
        Assert.notNull(acct, "Initialization failed");
    }

    @Test
    void initializeCurrentAccount() {
        CurrentAccount acct = new CurrentAccount(10L, UUID.randomUUID().toString(), 2000, 20000);
        Assert.notNull(acct, "Initialization failed");
    }


}
