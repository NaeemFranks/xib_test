package com.acme.test01.naeemfranks.service;

import com.acme.test01.naeemfranks.config.SystemDB;
import com.acme.test01.naeemfranks.exception.WithdrawalAmountTooLargeException;
import com.acme.test01.naeemfranks.modal.Account;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

class AccountServiceImplTest {

    private final AccountService acctService = new AccountServiceImpl();

    Account getAccount(long acctId) {
        return SystemDB.getAccount(acctId);
    }

    @Test
    void withdrawSavingsPass() throws WithdrawalAmountTooLargeException {
        long savingsAcctID = 1L;

        acctService.withdraw(savingsAcctID, 500);
        int expected = 1500;
        int actual = getAccount(savingsAcctID).getBalance();

        Assert.isTrue(expected == actual, String.format("Expected balance of %d. Got %d", expected, actual));
    }

    @Test
    void withdrawSavingsFail() {
        long savingsAcctID = 2L;

        try {
            acctService.withdraw(savingsAcctID, 10000);
        } catch (WithdrawalAmountTooLargeException ignored) {}

        int expected = 5000;
        int actual = getAccount(savingsAcctID).getBalance();

        Assert.isTrue(expected == actual, String.format("Expected balance of %d. Got %d", expected, actual));
    }

    @Test
    void withdrawCurrentPass() throws WithdrawalAmountTooLargeException {

        long currentAcctID = 3L;

        acctService.withdraw(currentAcctID, 5000);
        int expected = -4000;
        int actual = getAccount(currentAcctID).getBalance();

        Assert.isTrue(expected == actual, String.format("Expected balance of %d. Got %d", expected, actual));
    }

    @Test
    void withdrawCurrentFail() {

        long currentAcctID = 4L;

        try {
            acctService.withdraw(currentAcctID, 150000);
        } catch (Exception ignored) {}

        int actual = getAccount(currentAcctID).getBalance();
        int expected = 2000;

        Assert.isTrue(expected == actual, String.format("Expected balance of %d. Got %d", expected, actual));
    }

    @Test
    void deposit() {
        long currentAcctID = 4L;

        acctService.deposit(currentAcctID, 3000);

        int actual = getAccount(currentAcctID).getBalance();
        int expected = 5000;

        Assert.isTrue(expected == actual, String.format("Expected balance of %d. Got %d", expected, actual));
    }
}
