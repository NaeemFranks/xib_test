package com.acme.test01.naeemfranks.modal;

public class Account {

    private Long accountId;
    private String customerNumber;
    private int balance;

    public Account(Long accountId, String customerNumber, int balance) {
        this.accountId = accountId;
        this.customerNumber = customerNumber;
        this.balance = balance;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
