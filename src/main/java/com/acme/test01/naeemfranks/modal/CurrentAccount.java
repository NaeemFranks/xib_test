package com.acme.test01.naeemfranks.modal;

public class CurrentAccount extends Account {

    private int overdraft;

    public CurrentAccount(Long accountId, String customerNumber, int balance, int overdraft) {
        super(accountId, customerNumber, balance);
        this.overdraft = overdraft;
    }

    public int getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(int overdraft) {
        this.overdraft = overdraft;
    }
}
