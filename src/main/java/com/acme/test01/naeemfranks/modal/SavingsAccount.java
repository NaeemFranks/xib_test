package com.acme.test01.naeemfranks.modal;

public class SavingsAccount extends Account {

    public static final int MIN_BALANCE = 1000;


    public SavingsAccount(Long accountId, String customerNumber, int balance) {
        super(accountId, customerNumber, balance);
    }
}
