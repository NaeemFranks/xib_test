package com.acme.test01.naeemfranks.config;

import com.acme.test01.naeemfranks.exception.AccountNotFoundException;
import com.acme.test01.naeemfranks.modal.*;
import java.util.*;

public class SystemDB {

    private static final Map<Long, Account> accounts;


    static {
        accounts = new HashMap<>();
        long count = 0;

        accounts.put(++count, new SavingsAccount(count, UUID.randomUUID().toString(), 2000));
        accounts.put(++count, new SavingsAccount(count, UUID.randomUUID().toString(), 5000));
        accounts.put(++count, new CurrentAccount(count, UUID.randomUUID().toString(), 1000, 10000));
        accounts.put(++count, new CurrentAccount(count, UUID.randomUUID().toString(), 2000, 20000));
    }

    private SystemDB() {}

    public static Account getAccount(long accountId) {
        if (!accounts.containsKey(accountId))
            throw new AccountNotFoundException("Account with ID: " + accountId + " does not exist!");

        return accounts.get(accountId);
    }

    public static void save(Account acct) {
        accounts.replace(acct.getAccountId(), acct);
    }

}
