package com.acme.test01.naeemfranks.service;

import com.acme.test01.naeemfranks.config.SystemDB;
import com.acme.test01.naeemfranks.exception.AccountNotFoundException;
import com.acme.test01.naeemfranks.exception.WithdrawalAmountTooLargeException;
import com.acme.test01.naeemfranks.modal.Account;
import com.acme.test01.naeemfranks.modal.CurrentAccount;
import com.acme.test01.naeemfranks.modal.SavingsAccount;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class AccountServiceImpl implements AccountService {

    @Override
    public void openSavingsAccount(Long accountId, Long amountToDeposit) {
        throw new NotImplementedException();
    }

    @Override
    public void openCurrentAccount(Long accountId) {
        throw new NotImplementedException();
    }

    @Override
    public void withdraw(Long accountId, int amountToWithdraw) throws AccountNotFoundException, WithdrawalAmountTooLargeException {
        Account acct = SystemDB.getAccount(accountId);
        int newBalance = acct.getBalance() - amountToWithdraw;

        if (
                (acct instanceof SavingsAccount && newBalance < SavingsAccount.MIN_BALANCE) ||
                (acct instanceof CurrentAccount && newBalance < ((CurrentAccount) acct).getOverdraft() * -1)
        ) {
                throw new WithdrawalAmountTooLargeException("Insufficient funds");
        }

        acct.setBalance(newBalance);
        SystemDB.save(acct);

    }

    @Override
    public void deposit(Long accountId, int amountToDeposit) throws AccountNotFoundException {
        Account acct = SystemDB.getAccount(accountId);

        acct.setBalance(acct.getBalance() + amountToDeposit);
        SystemDB.save(acct);

    }
}
